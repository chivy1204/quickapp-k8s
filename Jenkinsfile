pipeline {
    agent {
        node {
            label "master"
        }
    }
    environment {
        BUILD_ID = "${env.BUILD_ID}";
        NEXUS_URL = "34.72.187.15:8081";
    }
    stages {
        stage('Checkout') {
            steps {
                script {
                    env.API = readFile '/tmp/webapi'
                }
                checkout([$class: 'GitSCM', branches: [[name: '*/master']],
                            extensions: [], userRemoteConfigs: [[credentialsId: 'gitlab',
                            url: 'https://gitlab.com/chivy1204/quickapp-k8s.git']]])
                sh '''
                    sed -i '' "s/example.com.vn/$(cat /tmp/webapi)/g" quickapp-ingress.yaml
                '''
            }
        }
        stage("Sonar Scan") {
            steps {
                sh '''
                    cd QuickApp
                    dotnet sonarscanner begin /k:"ScanAPI" /d:sonar.host.url="http://34.66.191.23"  /d:sonar.login="f31193a30cacb3ea3887692fe2c9a5b7537b7a53"
                    dotnet build
                    dotnet sonarscanner end /d:sonar.login="f31193a30cacb3ea3887692fe2c9a5b7537b7a53"
                '''
            }
        }
        stage('Unit Test') {
            steps {
                warnError('Unstable Tests') {
                    sh "cd QuickApp.Tests && dotnet test --logger:trx"
                }
                script {
                    allure([
                                includeProperties: false,
                                jdk: '',
                                properties: [],
                                reportBuildPolicy: 'ALWAYS',
                                results: [[path: 'QuickApp.Tests/TestResults']]
                    ])
                }
                zip dir: "allure-report", exclude: '', glob: '', zipFile: "allure-report.zip"
                nexusArtifactUploader artifacts: [[
                    artifactId: 'allure-report', classifier: '', file: 'allure-report.zip', type: 'zip'
                    ]], credentialsId: 'nexus-google-official',
                    groupId: 'allure-report',
                    nexusUrl: "$NEXUS_URL",
                    nexusVersion: 'nexus3',
                    protocol: 'http',
                    repository: 'allure-official',
                    version: '$BUILD_ID'
                sh '''
                    pwd
                    rm allure-report.zip
                    rm -r allure-report
                    cd QuickApp.Tests
                    rm -r TestResults
                '''
            }
        }
        stage('Build docker image') {
            agent {
                node {
                    label "azure-docker"
                }
            }
            steps {
                checkout([$class: 'GitSCM', branches: [[name: '*/master']],
                            extensions: [], userRemoteConfigs: [[credentialsId: 'gitlab',
                            url: 'https://gitlab.com/chivy1204/quickapp-k8s.git']]])
                sh '''
                    cd QuickApp/ClientApp/src/environments
                    sed -i "s/example.com.vn/$(echo $API)/g" environment.dev.ts
                    sed -i "s/example.com.vn/$(echo $API)/g" environment.ts
                    sed -i "s/example.com.vn/$(echo $API)/g" environment.prod.ts
                '''
                sh 'docker build -t quickapp-api .'
                sh "docker tag quickapp-api:latest vync/quickapp-api:v1.${BUILD_ID}"
                sh "docker tag quickapp-api:latest vync/quickapp-api:latest"
                sh "docker push vync/quickapp-api:v1.${BUILD_ID}"
                sh "docker push vync/quickapp-api:latest"
                sh "docker image rm -f quickapp-api:latest"

                sh '''
                    cd QuickApp/ClientApp
                    docker build -t quickapp-app .
                    docker tag quickapp-app:latest vync/quickapp-app:v1.${BUILD_ID}
                    docker tag quickapp-app:latest vync/quickapp-app:latest
                    docker push vync/quickapp-app:v1.${BUILD_ID}
                    docker push vync/quickapp-app:latest
                '''
                sh "docker image rm -f quickapp-app:latest"
            }
        }
        stage('Deploy to k8s') {
            agent {
                node {
                    label "master"
                }
            }
            steps {
                sh '''
                    cd /Users/vync/.jenkins/workspace/quickapp-k8s
                    kubectl apply -f cluster-issuer.yaml --namespace ingress-basic
                    kubectl apply -f webapi.yaml --namespace ingress-basic
                    kubectl apply -f quickapp-ingress.yaml --namespace ingress-basic
                    kubectl apply -f webapp.yaml --namespace ingress-basic
                '''
            }
        }
    }
}
