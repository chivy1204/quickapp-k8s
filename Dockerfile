FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
ENV ASPNETCORE_Kestrel__Certificates__Default__Path=/app/QuickApp.pfx 
ENV ASPNETCORE_HTTPS_PORT=4001
ENV ASPNETCORE_Kestrel__Certificates__Default__Password=tinh123
ENV ASPNETCORE_URLS=http://*:4000;https://*:4001
EXPOSE 4000 4001

 
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["QuickApp/QuickApp.csproj", "QuickApp/"]
COPY ["DAL/DAL.csproj", "DAL/"]
RUN dotnet restore "QuickApp/QuickApp.csproj"
COPY . .
WORKDIR "/src/QuickApp"
RUN dotnet build "QuickApp.csproj" -c Release -o /app/build
RUN dotnet dev-certs https --clean 
RUN dotnet dev-certs https -ep ./QuickApp.pfx -p tinh123
COPY ./QuickApp .
 
FROM build AS publish
RUN dotnet publish "QuickApp.csproj" -c Release -o /app/publish
 
FROM base AS final

WORKDIR /app
COPY --from=publish /app/publish .
COPY --from=build /src/QuickApp/QuickApp.pfx ./QuickApp.pfx
ENTRYPOINT ["dotnet","QuickApp.dll", "watch", "run", "--no-restore", "--urls", "http://0.0.0.0:4000"]